from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from navodayas.models import Navodaya

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=64, null=False, blank=False)
    image = models.ImageField(null = True, blank = True, upload_to = 'static/assets/uploads/images/profiles')
    bio = models.TextField(max_length=500, blank=True)
    jnv = models.ForeignKey(Navodaya, blank = True, null=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    passing_year = models.CharField(max_length=4, default=2018)
    email_confirmed = models.BooleanField(default=False)
    contact_no = models.CharField(max_length=10, null=False, blank=False, default='0000000000')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return str(self.full_name)
    def save(self, *args, **kwargs):
        self.full_name = self.user.first_name + " " + self.user.last_name
        return super(Profile, self).save(*args, **kwargs)
    
@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

class Education(models.Model):
    profile = models.ForeignKey(Profile, blank=False, null=False)
    title = models.CharField(max_length=64, blank=False, null=False)
    description = models.CharField(max_length=256, blank=False, null=False)
    institute = models.CharField(max_length=64, blank=False, null=False)
    start = models.DateField()
    finish = models.DateField()
    def __str__(self):
        return str(str(self.profile) + " " + self.title)

class Achievement(models.Model):
    profile = models.ForeignKey(Profile, blank=False, null=False)
    title = models.CharField(max_length=64, blank=False, null=False)
    description = models.CharField(max_length=256, blank=False, null=False)
    date = models.DateField()
    def __str__(self):
        return str(str(self.profile) + " " + self.title)