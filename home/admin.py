from django.contrib import admin
from . import models

# Register your models here.
@admin.register(models.Profile)
class ProfileAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">account_box</i>'
    list_display = ('user', 'jnv', 'location', 'passing_year', 'email_confirmed')

@admin.register(models.Education)
class EducationAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">library_books</i>'
    list_display = ('profile', 'title', 'institute')

@admin.register(models.Achievement)
class AchievementAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">redeem</i>'
    list_display = ('profile', 'title', 'description')