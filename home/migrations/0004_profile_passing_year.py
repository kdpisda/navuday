# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-18 10:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_profile_jnv'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='passing_year',
            field=models.CharField(default=2018, max_length=4),
        ),
    ]
