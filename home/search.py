from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date
from . import models

connections.create_connection()

class ProfileIndex(DocType):
    author = Text()
    posted_date = Date()
    title = Text()
    text = Text()

    class Meta:
        index = 'profile-index'