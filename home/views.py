from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect, HttpResponse
from django.http import JsonResponse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import logout
from home.forms import SignUpForm
from home.tokens import account_activation_token
from home.models import Profile, Education, Achievement
from navodayas.models import Navodaya

def signup(request):
    if request.user.is_authenticated:
        return redirect('/')
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = 'Activate Your MySite Account'
            message = render_to_string('account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)
            return redirect('account_activation_sent')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('home')
    else:
        return render(request, 'account_activation_invalid.html')

# @login_required
def home(request):
    return render(request, 'core/home.html')

@login_required
def show_user_profile(request):
    current_profile = Profile.objects.get(user = request.user)
    print (current_profile)
    educations = Education.objects.filter(profile = current_profile)
    achievements = Achievement.objects.filter(profile = current_profile)
    navodayas = Navodaya.objects.filter(status = True)
    context = {
        'educations' : educations,
        'achievements' : achievements,
        'profile' : current_profile,
        'navodayas' : navodayas
    }
    return render(request, 'core/profile.html', context)

@login_required
def update_name(request):
    try:
        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')
        temp_user = User.objects.get(username = request.user)
        temp_user.first_name = first_name
        temp_user.last_name = last_name
        temp_user.save()
        temp_profile = Profile.objects.get(user = temp_user)
        temp_profile.full_name = first_name + " " + last_name
        temp_profile.save()
        context = {
            'success' : True,
            'message' : 'Name updated successfully'
        }
    except Exception as e:
        print(e)
        context = {
            'success' : False,
            'message' : "Sorry asn error occured"
        }
    return JsonResponse(context, safe=False)

@login_required
def update_bio(request):
    try:
        bio = request.POST.get('bio')
        jnv_id = request.POST.get('jnv_id')
        location = request.POST.get('location')
        passing_year = request.POST.get('passing_year')
        temp_profile = Profile.objects.get(user = request.user)
        temp_profile.bio = bio
        temp_profile.jnv = Navodaya.objects.get(pk=jnv_id)
        temp_profile.location = location
        temp_profile.passing_year = passing_year
        temp_profile.save()
        context = {
            'success' : True,
            'message' : 'Bio updated successfully'
        }
    except Exception as e:
        print(e)
        context = {
            'success' : False,
            'message' : "Sorry an error occured"
        }
    return JsonResponse(context, safe=False)

@login_required
def update_dob(request):
    try:
        dob = request.POST.get('dob')
        temp_profile = Profile.objects.get(user = request.user)
        temp_profile.birth_date = dob
        temp_profile.save()
        context = {
            'success' : True,
            'message' : 'Bio updated successfully'
        }
    except Exception as e:
        print(e)
        context = {
            'success' : False,
            'message' : "Sorry an error occured"
        }
    return JsonResponse(context, safe=False)

@login_required
def add_education(request):
    if request.method == "POST" :
        temp_profile = Profile.objects.get(user = request.user)
        title = request.POST.get('title')
        description = request.POST.get('description')
        institute = request.POST.get('institute')
        startdate = request.POST.get('startdate')
        enddate = request.POST.get('enddate')
        temp_education = Education.objects.create(profile = temp_profile, title = title, description = description, institute = institute, start = startdate, finish = enddate)
        temp_education.save()
    return redirect('/profile')

@login_required
def add_achievement(request):
    if request.method == "POST" :
        temp_profile = Profile.objects.get(user = request.user)
        title = request.POST.get('title')
        description = request.POST.get('description')
        date = request.POST.get('date')        
        temp_achievement = Achievement.objects.create(profile = temp_profile, title = title, description = description, date = date)
        temp_achievement.save()
    return redirect('/profile')

def show_profile(request, username):
    temp_user = User.objects.get(username = username)
    current_profile = Profile.objects.get(user = temp_user)
    educations = Education.objects.filter(profile = current_profile)
    achievements = Achievement.objects.filter(profile = current_profile)
    navodayas = Navodaya.objects.filter(status = True)
    context = {
        'educations' : educations,
        'achievements' : achievements,
        'profile' : current_profile,
        'navodayas' : navodayas
    }
    return render(request, 'core/show_profile.html', context)

def show_about_us(request):
    return render(request, 'core/aboutus.html')

def show_privacy_policy(request):
    return render(request, 'core/privacy.html')

def show_forum(request):
    return render(request, 'core/coming_soon.html')

def show_terms(request):
    return render(request, 'core/terms.html')

def logout_view(request):
    logout(request)
    return render(request, 'registration/logout.html')