"""navuday URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from home import views as home_views
from events import views as events_views
from search import views as search_view
from navodayas import views as navodayas_view
from api import views as api_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^signup/$', home_views.signup, name='signup'),
    # url(r'^account_activation_sent/$', home_views.account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',home_views.activate, name='activate'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', home_views.logout_view, name='logout'),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    url(r'^events/(?P<page_slug>[\w-]+)', events_views.show_event, name='event_show'),
    url(r'^search/(?P<search_query>[\w|\W]+)', search_view.show_results, name='search_results_show'),
    url(r'^events', events_views.show_events, name='all_events_show'),
    url(r'^navodayas', navodayas_view.show_navodayas, name='all_navodayas_show'),
    url(r'^profile/update/name', home_views.update_name, name='update_profile_name'),
    url(r'^profile/update/bio', home_views.update_bio, name='update_profile_bio'),
    url(r'^profile/update/dob', home_views.update_dob, name='update_profile_dob'),
    url(r'^education/add', home_views.add_education, name='add_education'),
    url(r'^achievement/add', home_views.add_achievement, name='add_achievement'),
    url(r'^profile/(?P<username>[\w-]+)', home_views.show_profile, name='profile_show'),
    url(r'^profile', home_views.show_user_profile, name='my_profile_show'),
    url(r'^aboutus', home_views.show_about_us, name='about_us_show'),
    url(r'^forum', home_views.show_forum, name='forum_show'),
    url(r'^privacy', home_views.show_privacy_policy, name='privacy_policy_show'),
    url(r'^terms', home_views.show_terms, name='terms_show'),
    url(r'^api/', include('api.urls')),
    url(r'^$', home_views.home, name='home'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
