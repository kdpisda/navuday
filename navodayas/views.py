from django.shortcuts import render
from .models import Navodaya
# Create your views here.

def show_navodayas(request):
    temp_navodayas = Navodaya.objects.filter(status = True)
    temp_navodayas = list(temp_navodayas)
    return render(request, 'core/navodayas.html', {'navodayas' : temp_navodayas})