from django.db import models

class Navodaya(models.Model):
    full_name = models.TextField(max_length = 256)
    district = models.CharField(max_length = 40)
    state = models.CharField(max_length = 40)
    country = models.CharField(max_length = 20)
    image = models.ImageField(null = True, blank = True, upload_to = 'static/assets/uploads/images/jnvs')
    website = models.CharField(max_length = 64)
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.full_name