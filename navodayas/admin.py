from django.contrib import admin
from . import models

# Register your models here.
@admin.register(models.Navodaya)
class NavodayaAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">account_balance</i>'
    list_display = ('full_name', 'district', 'state', 'country', 'image', 'website', 'status')