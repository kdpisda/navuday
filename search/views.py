from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from events.models import Event
from django.contrib.auth.models import User
from home.models import Profile
from home.models import Education
import urllib

# Create your views here.
def show_results(request, search_query):
    events_id = []
    users_id = []
    search_result = {}
    search_elements = []
    search_query = urllib.parse.unquote(search_query)

    # Searching Events with its title
    search_events_exist = Event.objects.filter(title__search = search_query).exists()
    if search_events_exist:
        temp_events = Event.objects.filter(title__search = search_query)
        for temp_event in temp_events:
            if not (temp_event.pk in events_id):
                search_element = {
                    'type' : 'event',
                    'title' : temp_event.title,
                    'description' : temp_event.description,
                    'time' : temp_event.time,
                    'venue' : temp_event.place,
                    'image' : str(temp_event.image),
                    'id' : temp_event.pk,
                    'link' : '/events/'+temp_event.slug
                }
                events_id.append(temp_event.pk)
                search_elements.append(search_element)

    # searching Events with its place
    search_events_exist = Event.objects.filter(place__search = search_query).exists()
    if search_events_exist:
        temp_events = Event.objects.filter(place__search = search_query)
        for temp_event in temp_events:
            if not (temp_event.pk in events_id):
                search_element = {
                    'type' : 'event',
                    'title' : temp_event.title,
                    'description' : temp_event.description,
                    'time' : temp_event.time,
                    'venue' : temp_event.place,
                    'image' : str(temp_event.image),
                    'id' : temp_event.pk,
                    'link' : '/events/'+temp_event.slug
                }
                events_id.append(temp_event.pk)
                search_elements.append(search_element)

    # searching Events with its description
    search_events_exist = Event.objects.filter(description__search = search_query).exists()
    if search_events_exist:
        temp_events = Event.objects.filter(description__search = search_query)
        for temp_event in temp_events:
            if not (temp_event.pk in events_id):
                search_element = {
                    'type' : 'event',
                    'title' : temp_event.title,
                    'description' : temp_event.description,
                    'time' : temp_event.time,
                    'venue' : temp_event.place,
                    'image' : str(temp_event.image),
                    'id' : temp_event.pk,
                    'link' : '/events/'+temp_event.slug
                }
                events_id.append(temp_event.pk)
                search_elements.append(search_element)
    
    # searching Users with its place
    search_users_exist = Profile.objects.filter(location__search = search_query).exists()
    if search_users_exist:
        temp_profiles = Profile.objects.filter(location__search = search_query)
        for temp_profile in temp_profiles:
            if not(temp_profile.pk in users_id):
                if str(temp_profile.image) == "":
                    profile_image = 'static/assets/images/logo.png'
                else:
                    profile_image = str(temp_profile.image)
                search_element = {
                    'type' : 'profile',
                    'title' : temp_profile.user.first_name + " " +temp_profile.user.last_name,
                    'bio' : temp_profile.bio,
                    'image' : profile_image,
                    'bio' : temp_profile.bio,
                    'jnv' : str(temp_profile.jnv),
                    'passing_year' : temp_profile.passing_year,
                    'link' : '/profile/'+str(temp_profile.user)
                }
                users_id.append(temp_profile.pk)
                search_elements.append(search_element)

    # searching Users with its bio
    search_users_exist = Profile.objects.filter(bio__search = search_query).exists()
    if search_users_exist:
        temp_profiles = Profile.objects.filter(bio__search = search_query)
        for temp_profile in temp_profiles:
            if not(temp_profile.pk in users_id):
                if str(temp_profile.image) == "":
                    profile_image = 'static/assets/images/logo.png'
                else:
                    profile_image = str(temp_profile.image)
                search_element = {
                    'type' : 'profile',
                    'title' : temp_profile.user.first_name + " " +temp_profile.user.last_name,
                    'bio' : temp_profile.bio,
                    'image' : profile_image,
                    'bio' : temp_profile.bio,
                    'jnv' : str(temp_profile.jnv),
                    'passing_year' : temp_profile.passing_year,
                    'link' : '/profile/'+str(temp_profile.user)
                }
                users_id.append(temp_profile.pk)
                search_elements.append(search_element)

    # searching Users with its passing year
    search_users_exist = Profile.objects.filter(passing_year__search = search_query).exists()
    if search_users_exist:
        temp_profiles = Profile.objects.filter(passing_year__search = search_query)
        for temp_profile in temp_profiles:
            if not(temp_profile.pk in users_id):
                if str(temp_profile.image) == "":
                    profile_image = 'static/assets/images/logo.png'
                else:
                    profile_image = str(temp_profile.image)
                search_element = {
                    'type' : 'profile',
                    'title' : temp_profile.user.first_name + " " +temp_profile.user.last_name,
                    'bio' : temp_profile.bio,
                    'image' : profile_image,
                    'bio' : temp_profile.bio,
                    'jnv' : str(temp_profile.jnv),
                    'passing_year' : temp_profile.passing_year,
                    'link' : '/profile/'+str(temp_profile.user)
                }
                users_id.append(temp_profile.pk)
                search_elements.append(search_element)
    
    # searching Users with its JNV
    search_users_exist = Education.objects.filter(institute__search = search_query).exists()
    if search_users_exist:
        temp_educations = Education.objects.filter(institute__search = search_query)
        for temp_education in temp_educations:
            if not(temp_education.profile.pk in users_id):
                if str(temp_profile.image) == "":
                    profile_image = 'static/assets/images/logo.png'
                else:
                    profile_image = str(temp_profile.image)
                search_element = {
                    'type' : 'profile',
                    'title' : temp_education.profile.user.first_name + " " +temp_education.profile.user.last_name,
                    'bio' : temp_education.profile.bio,
                    'image' : str(profile_imageimage),
                    'bio' : temp_education.profile.bio,
                    'jnv' : str(temp_education.profile.jnv),
                    'passing_year' : temp_education.profile.passing_year,
                    'link' : '/profile/'+str(temp_education.profile.user)
                }
                users_id.append(temp_education.profile.pk)
                search_elements.append(search_element)

    # searching Users with its Full Name
    search_users_exist = Profile.objects.filter(full_name__search = search_query).exists()
    if search_users_exist:
        temp_profiles = Profile.objects.filter(full_name__search = search_query)
        for temp_profile in temp_profiles:
            if not(temp_profile.pk in users_id):
                if str(temp_profile.image) == "":
                    profile_image = 'static/assets/images/logo.png'
                else:
                    profile_image = str(temp_profile.image)
                search_element = {
                    'type' : 'profile',
                    'title' : temp_profile.user.first_name + " " +temp_profile.user.last_name,
                    'bio' : temp_profile.bio,
                    'image' : profile_image,
                    'bio' : temp_profile.bio,
                    'jnv' : str(temp_profile.jnv),
                    'passing_year' : temp_profile.passing_year,
                    'link' : '/profile/'+str(temp_profile.user)
                }
                users_id.append(temp_profile.pk)
                search_elements.append(search_element)

    # searching Users with its first name
    search_users_exist = User.objects.filter(first_name__search = search_query).exists()
    if search_users_exist:
        temp_users = User.objects.filter(first_name__search = search_query)
        for temp_user in temp_users:
            temp_profile = Profile.objects.get(user = temp_user)
            if not(temp_profile.pk in users_id):
                if str(temp_profile.image) == "":
                    profile_image = 'static/assets/images/logo.png'
                else:
                    profile_image = str(temp_profile.image)
                search_element = {
                    'type' : 'profile',
                    'title' : temp_profile.user.first_name + " " +temp_profile.user.last_name,
                    'bio' : temp_profile.bio,
                    'image' : profile_image,
                    'bio' : temp_profile.bio,
                    'jnv' : str(temp_profile.jnv),
                    'passing_year' : temp_profile.passing_year,
                    'link' : '/profile/'+str(temp_profile.user)
                }
                users_id.append(temp_profile.pk)
                search_elements.append(search_element)

    # Searching Users with its last name
    search_users_exist = User.objects.filter(last_name__search = search_query).exists()
    if search_users_exist:
        temp_users = User.objects.filter(last_name__search = search_query)
        for temp_user in temp_users:
            temp_profile = Profile.objects.get(user = temp_user)
            if not(temp_profile.pk in users_id):
                if str(temp_profile.image) == "":
                    profile_image = 'static/assets/images/logo.png'
                else:
                    profile_image = str(temp_profile.image)
                search_element = {
                    'type' : 'profile',
                    'title' : temp_profile.user.first_name + " " +temp_profile.user.last_name,
                    'bio' : temp_profile.bio,
                    'image' : profile_image,
                    'bio' : temp_profile.bio,
                    'jnv' : str(temp_profile.jnv),
                    'passing_year' : temp_profile.passing_year,
                    'link' : '/profile/'+str(temp_profile.user)
                }
                users_id.append(temp_profile.pk)
                search_elements.append(search_element)

    if len(search_elements) == 0:
        search_result = {
            'success' : False,
            'message' : 'Sorry we could not found any results'
        }
    else:
        search_result = {
            'success' : True,
            'results' : search_elements
        }
    return JsonResponse(search_result, safe=False)