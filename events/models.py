from django.db import models
from django.template.defaultfilters import slugify

# Create your models here.
class Event(models.Model):
    title = models.CharField(max_length=40)
    status = models.BooleanField()
    place = models.CharField(null = False, blank = False, max_length = 64)
    slug = models.SlugField(unique=True, null=True, blank=True)
    description = models.TextField()
    time = models.DateTimeField()
    timing = models.DurationField()
    image = models.ImageField(upload_to = 'static/assets/uploads/images/events')
    document = models.FileField(null=True, blank=True, upload_to = 'static/assets/uploads/docs')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        return super(Event, self).save(*args, **kwargs)
    def __str__(self):
        return self.title