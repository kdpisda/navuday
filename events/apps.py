from django.apps import AppConfig
from material.frontend.apps import ModuleMixin

class Events(ModuleMixin, AppConfig):
    name = 'events'
    icon = '<i class="material-icons">event</i>'
