from django.shortcuts import render, get_object_or_404
from .models import Event
# Create your views here.

def show_events(request):
    temp_events = Event.objects.filter(status = True)
    temp_events = list(temp_events)
    return render(request, 'core/events.html', {'events' : temp_events})

def show_event(request, page_slug):
    temp_event = get_object_or_404(Event, slug=page_slug)
    return render(request, 'core/event_show.html', { 'event' : temp_event})