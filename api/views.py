from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from home.models import Profile

# Create your views here.
def get_profile(request, id):
    try:
        temp_profile = Profile.objects.get(pk=id)
        if request.user == temp_profile.user:
            # This means the profile of the user is same as the logged user
            context = {
                'success' : True
            }
        else: 
            # API must not give details of the user since the user is not the same
            context = {
                'success' : False
            }
    except Exception as e:
        print(e)
        context = {
            'success' : False,
            'message' : 'Sorry an error occured'
        }
    return JsonResponse(context, safe=False)