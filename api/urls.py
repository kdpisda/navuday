from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from api.views import get_profile

urlpatterns = [
    url(r'^profile/(?P<id>[0-9]+)', get_profile),
]